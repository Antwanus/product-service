package com.antoonvereecken.productservice.domain;

import com.antoonvereecken.common.model.ProductCategory;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Product {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Type(type = "org.hibernate.type.UUIDCharType")
    @Column(length = 36, columnDefinition = "varchar(36)", updatable = false, nullable = false)
    private UUID id;

    @Version
    private Long version;

    @CreationTimestamp
    @Column(updatable = false)
    private Timestamp createdDate;

    @UpdateTimestamp
    private Timestamp lastModifiedDate;

    @Column(unique = true, length = 13, columnDefinition = "varchar(13)")
    private String eanId;
    @Column(unique = true, length = 50, columnDefinition = "varchar(50)")
    private String productName;
    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private ProductCategory productCategory;

    private BigDecimal productPrice;
    private Integer minStock;
    private Integer productionQuantity;


}
