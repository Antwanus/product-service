package com.antoonvereecken.productservice.bootstrap;

import com.antoonvereecken.common.model.ProductCategory;
import com.antoonvereecken.productservice.domain.Product;
import com.antoonvereecken.productservice.repository.ProductRepo;
import org.springframework.boot.CommandLineRunner;

import java.math.BigDecimal;

// USING IMPORT.SQL (to make sure I use the same UUID between services since UUID is auto generated)
// @Component
public class ProductLoader implements CommandLineRunner {

    public static final String PRODUCT_1_EAN = "1111111111111";
    public static final String PRODUCT_2_EAN = "2222222222222";
    public static final String PRODUCT_3_EAN = "3333333333333";

    public static final String PRODUCT_PATH_V1 = "/api/v1/product/";
    public static final String PRODUCT_EAN_PATH_V1 = "/api/v1/product/ean/";

    private final ProductRepo repo;

    public ProductLoader(ProductRepo repo) {
        this.repo = repo;
    }

    @Override
    public void run(String... args) {
        loadTestData();
    }

    private void loadTestData() {
        if(repo.count() == 0) {

            repo.save(Product.builder()
                    .productName("strawberry")
                    .productCategory(ProductCategory.FRUIT)
                    .minStock(100)
                    .productionQuantity(1000)
                    .eanId(PRODUCT_1_EAN)
                    .productPrice(new BigDecimal("3.5"))
                    .build());

            repo.save(Product.builder()
                    .productName("banana")
                    .productCategory(ProductCategory.FRUIT)
                    .minStock(100)
                    .productionQuantity(1000)
                    .eanId(PRODUCT_2_EAN)
                    .productPrice(new BigDecimal("3.5"))
                    .build());

            repo.save(Product.builder()
                    .productName("mushroom")
                    .productCategory(ProductCategory.VEGETABLE)
                    .minStock(10)
                    .productionQuantity(30)
                    .eanId(PRODUCT_3_EAN)
                    .productPrice(new BigDecimal("3.5"))
                    .build());

        }

    }
}
