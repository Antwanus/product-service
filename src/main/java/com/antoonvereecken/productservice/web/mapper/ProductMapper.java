package com.antoonvereecken.productservice.web.mapper;


import com.antoonvereecken.common.model.ProductDto;
import com.antoonvereecken.productservice.domain.Product;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;

@Mapper(uses = { DateMapper.class })
@DecoratedWith( ProductMapperDecorator.class )
public interface ProductMapper {

    ProductDto productToDto(Product product);
    ProductDto productToDtoWithInventory(Product product);

    Product dtoToProduct(ProductDto dto);

}
