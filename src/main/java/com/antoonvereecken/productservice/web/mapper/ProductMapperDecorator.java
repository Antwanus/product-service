package com.antoonvereecken.productservice.web.mapper;

import com.antoonvereecken.common.model.ProductDto;
import com.antoonvereecken.productservice.domain.Product;
import com.antoonvereecken.productservice.service.inventory.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class ProductMapperDecorator implements ProductMapper{

    private InventoryService inventoryService;
    private ProductMapper mapper;

    @Autowired
    public void setInventoryService(InventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }
    @Autowired
    public void setMapper(ProductMapper mapper) {
        this.mapper = mapper;
    }


    @Override
    public ProductDto productToDto(Product product) {
        return mapper.productToDto(product);
    }

    @Override
    public Product dtoToProduct(ProductDto dto) {
        return mapper.dtoToProduct(dto);
    }

    @Override
    public ProductDto productToDtoWithInventory(Product product) {
        ProductDto dto = mapper.productToDto(product);
        dto.setCurrentStock(
                inventoryService.getCurrentStock(product.getId())
        );
        return dto;
    }
}
