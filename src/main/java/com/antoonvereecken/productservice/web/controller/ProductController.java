package com.antoonvereecken.productservice.web.controller;

import com.antoonvereecken.common.model.ProductCategory;
import com.antoonvereecken.common.model.ProductDto;
import com.antoonvereecken.common.model.ProductPagedList;
import com.antoonvereecken.productservice.service.product.ProductService;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RequestMapping(value = "/api/v1/product")
@RestController
public class ProductController {

    private static final Integer DEFAULT_PAGE_NUMBER = 0;
    private static final Integer DEFAULT_PAGE_SIZE = 25;
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public ResponseEntity<ProductPagedList> listAllProducts(
            @RequestParam(value = "number", required = false) Integer pageNumber,
            @RequestParam(value = "size", required = false) Integer pageSize,
            @RequestParam(value = "productName", required = false) String productName,
            @RequestParam(value = "productCategory", required = false) ProductCategory productCategory,
            @RequestParam(value = "showInventory", required = false, defaultValue = "false") Boolean showInventory
    ){
        if (pageNumber == null || pageNumber < 0){
            pageNumber = DEFAULT_PAGE_NUMBER;
        }
        if (pageSize == null || pageSize < 1) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        ProductPagedList productList = productService.listProducts(
                productName,
                productCategory,
                PageRequest.of(pageNumber, pageSize),
                showInventory
        );
        return new ResponseEntity<>(productList, HttpStatus.OK);
    }
    @GetMapping(value = "/{productId}")
    public ResponseEntity<ProductDto> getProductByProductId(@PathVariable("productId") UUID productId) {
        return new ResponseEntity<>(
                productService.getProductByProductId(productId, false),
                HttpStatus.OK
        );
    }
    @GetMapping(value = "/ean/{eanId}")
    public ResponseEntity<ProductDto> getProductByEanId(@PathVariable("eanId") String eanId) {
        return new ResponseEntity<>(
                productService.getProductByEanId(eanId),
                HttpStatus.OK
        );
    }
    @PostMapping
    public ResponseEntity<ProductDto> createProduct(@RequestBody @Validated ProductDto dto) {
        return new ResponseEntity<>(
                productService.saveNewProduct(dto),
                HttpStatus.CREATED
        );
    }
    @PutMapping(value = "/{productId}")
    public ResponseEntity<ProductDto> updateProduct(
            @PathVariable("productId") UUID productId,
            @RequestBody @Validated ProductDto dto
    ) {
        return new ResponseEntity<>(
                productService.updateProduct(productId, dto),
                HttpStatus.NO_CONTENT
        );
    }
}
