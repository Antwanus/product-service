package com.antoonvereecken.productservice.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SysController {

    @RequestMapping("/sysresources")
    public String getSystemResources() {
        long memory = Runtime.getRuntime().maxMemory();
        int cores = Runtime.getRuntime().availableProcessors();
        return "/SystemResources \n memory: " + memory / 1024 / 1024 + "\n cores: " + cores;
    }

}
