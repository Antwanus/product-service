package com.antoonvereecken.productservice.config;

import feign.auth.BasicAuthRequestInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignClientRequestInterceptorConfig {

    @Bean
    public BasicAuthRequestInterceptor basicAuthenticationInterceptor(
            @Value("${com.antoonvereecken.inventory-user:user}") String inventoryUser,
            @Value("${com.antoonvereecken.inventory-password:pw}") String inventoryPassword
    ) {
        return new BasicAuthRequestInterceptor(inventoryUser, inventoryPassword);
        //return new BasicAuthenticationInterceptor("user", "pw");
    }

}
