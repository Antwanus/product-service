package com.antoonvereecken.productservice.service.inventory;

import com.antoonvereecken.common.model.ProductInventoryDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Slf4j
@Profile("!local-discovery")
@ConfigurationProperties(prefix= "com.antoonvereecken", ignoreUnknownFields = true)
@Component
public class InventoryServiceImpl implements InventoryService {

    public static final String PRODUCT_INVENTORY_PATH = "/api/v1/product/{productId}/inventory";
    private final RestTemplate restTemplate;
    private String inventoryServiceHost;

    // application.properties: com.antoonvereecken.product-inventory-service-host="..."
    public void setProductInventoryServiceHost(String host) {
        this.inventoryServiceHost = host;
    }
    public InventoryServiceImpl(
            RestTemplateBuilder restTemplateBuilder,
            @Value("${com.antoonvereecken.inventory-user:user}") String inventoryUser,
            @Value("${com.antoonvereecken.inventory-password:pw}") String inventoryPassword

    ) {
        this.restTemplate = restTemplateBuilder
                .basicAuthentication(inventoryUser, inventoryPassword)
                //.basicAuthentication("user", "pw")
                .build();
    }

    @Override
    public Integer getCurrentStock(UUID productId) {
        log.info("---> CALLING INVENTORY-SERVICE");

        ResponseEntity<List<ProductInventoryDto>> responseEntity = restTemplate.exchange(
                inventoryServiceHost + PRODUCT_INVENTORY_PATH,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<ProductInventoryDto>>(){},
                (Object) productId
        );

        return Objects.requireNonNull(responseEntity.getBody())
                .stream()
                .mapToInt(ProductInventoryDto::getCurrentStock)
                .sum();
    }


}
