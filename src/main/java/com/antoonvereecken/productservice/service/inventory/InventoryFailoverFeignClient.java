package com.antoonvereecken.productservice.service.inventory;

import com.antoonvereecken.common.model.ProductInventoryDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(name = "inventory-failover-service")
public interface InventoryFailoverFeignClient {

    @GetMapping(value = "/inventory-failover")
    ResponseEntity<List<ProductInventoryDto>> getCurrentStock();


}
