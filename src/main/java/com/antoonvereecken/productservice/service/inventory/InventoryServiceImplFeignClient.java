package com.antoonvereecken.productservice.service.inventory;

import com.antoonvereecken.common.model.ProductInventoryDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Slf4j
@Profile("local-discovery")
@RequiredArgsConstructor
@Service
public class InventoryServiceImplFeignClient implements InventoryService {

    private final InventoryServiceFeignClient inventoryServiceFeignClient;

    @Override
    public Integer getCurrentStock(UUID productId) {
        log.debug("Calling inventory-service -> ProductId: " + productId);

        ResponseEntity<List<ProductInventoryDto>> responseEntity = inventoryServiceFeignClient.getCurrentStock(productId);

        Integer currentStock = Objects.requireNonNull(responseEntity.getBody())
                .stream()
                .mapToInt(ProductInventoryDto::getCurrentStock)
                .sum();

        log.debug("PRODUCT: " + productId + " -> CURRENT STOCK : " + currentStock);

        return currentStock;
    }
}
