package com.antoonvereecken.productservice.service.inventory;

import com.antoonvereecken.common.model.ProductInventoryDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Component
public class InventoryServiceFeignClientImpl implements InventoryServiceFeignClient{

    private final InventoryFailoverFeignClient failoverFeignClient;

    @Override
    public ResponseEntity<List<ProductInventoryDto>> getCurrentStock(UUID productId) {
        return failoverFeignClient.getCurrentStock();
    }


}
