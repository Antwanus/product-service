package com.antoonvereecken.productservice.service.inventory;

import com.antoonvereecken.common.model.ProductInventoryDto;
import com.antoonvereecken.productservice.config.FeignClientRequestInterceptorConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.UUID;

@FeignClient(
        name = "inventory-service",
        fallback = InventoryServiceFeignClientImpl.class,
        configuration = FeignClientRequestInterceptorConfig.class
)
public interface InventoryServiceFeignClient {

    @GetMapping(value = InventoryServiceImpl.PRODUCT_INVENTORY_PATH)
    ResponseEntity<List<ProductInventoryDto>> getCurrentStock(@PathVariable UUID productId);
}
