package com.antoonvereecken.productservice.service.inventory;

import java.util.UUID;

public interface InventoryService {

    Integer getCurrentStock(UUID productId);

}
