package com.antoonvereecken.productservice.service.listener;

import com.antoonvereecken.common.event.ValidateOrderRequest;
import com.antoonvereecken.common.event.ValidateOrderResult;
import com.antoonvereecken.productservice.config.JmsConfig;
import com.antoonvereecken.productservice.service.order.OrderValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class OrderValidationListener {

    private final OrderValidator validator;
    private final JmsTemplate jmsTemplate;

    @JmsListener(destination = JmsConfig.VALIDATE_ORDER_QUEUE)
    public void listen(ValidateOrderRequest validateOrderRequest) {
        Boolean isValid = validator.validateOrder(validateOrderRequest.getOrderDto());

        jmsTemplate.convertAndSend(
                JmsConfig.VALIDATE_ORDER_RESULT_QUEUE,
                ValidateOrderResult.builder()
                        .isValid(isValid)
                        .orderId(validateOrderRequest.getOrderDto().getId())
                        .build());

    }


}
