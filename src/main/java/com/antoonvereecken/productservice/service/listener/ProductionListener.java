package com.antoonvereecken.productservice.service.listener;

import com.antoonvereecken.common.event.NewInventoryEvent;
import com.antoonvereecken.common.event.ProductionEvent;
import com.antoonvereecken.common.model.ProductDto;
import com.antoonvereecken.productservice.config.JmsConfig;
import com.antoonvereecken.productservice.domain.Product;
import com.antoonvereecken.productservice.repository.ProductRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Service
public class ProductionListener {

    private final ProductRepo productRepo;
    private final JmsTemplate jmsTemplate;

    @Transactional
    @JmsListener(destination = JmsConfig.PRODUCTION_REQUEST_QUEUE)
    public void listen(ProductionEvent productionEvent) {
        ProductDto productDto = productionEvent.getProductDto();
        Product p = productRepo.getOne(productDto.getId());
        // update inventory
        productDto.setCurrentStock(p.getProductionQuantity());
        // send newInventoryEvent(productDto) to the queue
        NewInventoryEvent newInventoryEvent = new NewInventoryEvent(productDto);
        jmsTemplate.convertAndSend(JmsConfig.NEW_INVENTORY_QUEUE, newInventoryEvent);

    }

}
