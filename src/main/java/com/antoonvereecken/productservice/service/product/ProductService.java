package com.antoonvereecken.productservice.service.product;

import com.antoonvereecken.common.model.ProductCategory;
import com.antoonvereecken.common.model.ProductDto;
import com.antoonvereecken.common.model.ProductPagedList;
import org.springframework.data.domain.PageRequest;

import java.util.UUID;

public interface ProductService {

    ProductPagedList listProducts(String productName, ProductCategory productCategory, PageRequest pageRequest, Boolean showInventory);

    ProductDto getProductByProductId(UUID productId, Boolean showInventory);

    ProductDto getProductByEanId(String eanId);

    ProductDto saveNewProduct(ProductDto dto);

    ProductDto updateProduct(UUID productId, ProductDto dto);
}
