package com.antoonvereecken.productservice.service.product;

import com.antoonvereecken.common.model.ProductCategory;
import com.antoonvereecken.common.model.ProductDto;
import com.antoonvereecken.common.model.ProductPagedList;
import com.antoonvereecken.productservice.domain.Product;
import com.antoonvereecken.productservice.repository.ProductRepo;
import com.antoonvereecken.productservice.web.controller.exception.NotFoundException;
import com.antoonvereecken.productservice.web.mapper.ProductMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepo repo;
    private final ProductMapper mapper;

    @Cacheable(cacheNames = "productListCache", condition = "#showInventory == false")
    @Override
    public ProductPagedList listProducts(
            String productName,
            ProductCategory productCategory,
            PageRequest pageRequest,
            Boolean showInventory
    ) {
        log.info("|>|>|> listProduct() has been called (=> response was NOT cached) <|<|<|");
        ProductPagedList productPagedList;
        Page<Product> productPage;

        productPage = createPageChooseRepoMethodByParams(productName, productCategory, pageRequest);

        productPagedList = createPagedListWithOrWithoutInventory(showInventory, productPage);

        return productPagedList;
    }

    @NotNull
    private ProductPagedList createPagedListWithOrWithoutInventory(Boolean showInventory, Page<Product> productPage) {
        ProductPagedList productPagedList;
        if (Boolean.TRUE.equals(showInventory)) {
            productPagedList = new ProductPagedList(
                    productPage.getContent()
                        .stream()
                        .map(mapper::productToDtoWithInventory)
                        .collect(Collectors.toList()
                        ),
                    PageRequest
                            .of(
                                    productPage.getPageable().getPageNumber(),
                                    productPage.getPageable().getPageSize()
                            ),
                    productPage.getTotalElements());
        } else {
            productPagedList = new ProductPagedList(
                    productPage.getContent()
                        .stream()
                        .map(mapper::productToDto)
                        .collect(Collectors.toList()
                        ),
                    PageRequest
                        .of(
                                productPage.getPageable().getPageNumber(),
                                productPage.getPageable().getPageSize()
                        ),
                        productPage.getTotalElements());
        }
        return productPagedList;
    }
    private Page<Product> createPageChooseRepoMethodByParams(String productName, ProductCategory productCategory, PageRequest pageRequest) {
        Page<Product> productPage;
        if(!StringUtils.isEmpty(productName) && !StringUtils.isEmpty(productCategory)) {
            productPage = repo.findAllByProductNameAndProductCategory(productName, productCategory, pageRequest);

        } else if(!StringUtils.isEmpty(productName) && StringUtils.isEmpty(productCategory)) {
            productPage = repo.findAllByProductName(productName, pageRequest);

        } else if(StringUtils.isEmpty(productName) && !StringUtils.isEmpty(productCategory)) {
            productPage = repo.findAllByProductCategory(productCategory, pageRequest);

        } else {
            productPage = repo.findAll(pageRequest);
        }
        return productPage;
    }

    @Cacheable(cacheNames = "productCache", key = "#productId", condition = "#showInventory == false ")
    @Override
    public ProductDto getProductByProductId(UUID productId, Boolean showInventory) {
        log.info("|>|>|> getProductByProductId() has been called (=> response was NOT cached) <|<|<|");
        if (Boolean.TRUE.equals(showInventory)) {
            return mapper.productToDtoWithInventory(
                    repo.findById(productId).orElseThrow(NotFoundException::new)
            );
        } else
            return mapper.productToDto(
                    repo.findById(productId).orElseThrow(NotFoundException::new)
            );
    }

    @Cacheable(cacheNames = "productEanCache", key = "#eanId", condition = "#showInventory == false ")
    @Override
    public ProductDto getProductByEanId(String eanId) {
        log.info("|>|>|> getProductByEanId() has been called (=> response was NOT cached) <|<|<|");
        return mapper.productToDto(repo.findByEanId(eanId));
    }

    @Override
    public ProductDto saveNewProduct(ProductDto dto) {
        Product product = mapper.dtoToProduct(dto);

        return mapper.productToDto(repo.save(product));
    }

    @Override
    public ProductDto updateProduct(UUID productId, ProductDto dto) {
        Product p = repo.findById(productId).orElseThrow(NotFoundException::new);
        p.setEanId(dto.getEanId());
        p.setProductName(dto.getProductName());
        p.setProductCategory(dto.getProductCategory());
        p.setProductPrice(dto.getProductPrice());

        repo.saveAndFlush(p);

        return mapper.productToDto(p);
    }


}
