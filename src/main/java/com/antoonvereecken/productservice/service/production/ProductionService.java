package com.antoonvereecken.productservice.service.production;

import com.antoonvereecken.common.event.ProductionEvent;
import com.antoonvereecken.productservice.config.JmsConfig;
import com.antoonvereecken.productservice.domain.Product;
import com.antoonvereecken.productservice.repository.ProductRepo;
import com.antoonvereecken.productservice.service.inventory.InventoryService;
import com.antoonvereecken.productservice.web.mapper.ProductMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class ProductionService {

    private final ProductRepo productRepo;
    private final InventoryService inventoryService;
    private final JmsTemplate jmsTemplate;
    private final ProductMapper mapper;

    @Scheduled(fixedRate = 30000)
    public void checkForLowInventory() {

        for (Product product : productRepo.findAll()) {
            Integer currentStock = inventoryService.getCurrentStock(product.getId());
            log.debug(  "ProductionService.checkForLowInventory() |>|>|> MINIMUM: "
                        + product.getMinStock() + " --> CURRENT: " + currentStock);
            if ( currentStock <= product.getMinStock() ) {
                jmsTemplate.convertAndSend(
                        JmsConfig.PRODUCTION_REQUEST_QUEUE,
                        new ProductionEvent(mapper.productToDto(product))
                );
                log.debug("CURRENT < MINIMUM for EAN " + product.getEanId() + " -> SENDING ProductionEvent(Product) to the QUEUE");
            }}
    }

}
