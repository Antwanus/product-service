package com.antoonvereecken.productservice.service.order;

import com.antoonvereecken.common.model.ProductOrderDto;
import com.antoonvereecken.productservice.domain.Product;
import com.antoonvereecken.productservice.repository.ProductRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@RequiredArgsConstructor
@Component
public class OrderValidator {

    private final ProductRepo productRepo;

    public Boolean validateOrder(ProductOrderDto orderDto) {

        AtomicInteger numberOfProductsNotFound = new AtomicInteger();

        orderDto.getProductOrderLines().forEach(line -> {
            Product p = productRepo.findByEanId(line.getEanId());
            log.info("Looking up " + line.getEanId() + "... found " + p.getEanId());
            if(p.getEanId() == null) {
                numberOfProductsNotFound.incrementAndGet();
            }

        });

        return numberOfProductsNotFound.get() == 0; // true (= everything is found)
    }

}
