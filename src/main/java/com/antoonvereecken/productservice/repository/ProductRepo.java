package com.antoonvereecken.productservice.repository;

import com.antoonvereecken.common.model.ProductCategory;
import com.antoonvereecken.productservice.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ProductRepo extends JpaRepository<Product, UUID> {

    Page<Product> findAllByProductName(String beerName, Pageable pageable);

    Page<Product> findAllByProductCategory(ProductCategory productCategory, Pageable pageable);

    Page<Product> findAllByProductNameAndProductCategory(String productName, ProductCategory productCategory, Pageable pageable);

    Product findByEanId(String eanId);

}
