package com.antoonvereecken.common.event;

import com.antoonvereecken.common.model.ProductDto;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ProductionEvent extends ProductEvent{

    public ProductionEvent(ProductDto productDto) {
        super(productDto);
    }
}
