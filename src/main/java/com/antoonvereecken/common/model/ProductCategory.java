package com.antoonvereecken.common.model;

public enum ProductCategory {

    VEGETABLE, FRUIT

}
