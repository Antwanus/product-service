package com.antoonvereecken.productservice.controller;

import com.antoonvereecken.common.model.ProductCategory;
import com.antoonvereecken.common.model.ProductDto;
import com.antoonvereecken.common.model.ProductPagedList;
import com.antoonvereecken.productservice.bootstrap.ProductLoader;
import com.antoonvereecken.productservice.service.product.ProductService;
import com.antoonvereecken.productservice.web.controller.ProductController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.constraints.ConstraintDescriptions;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.restdocs.snippet.Attributes.key;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Tag("controller")
@DisplayName("ProductController { ")
@AutoConfigureRestDocs(uriScheme = "https", uriHost = "antwanus", uriPort = 8085)
@WebMvcTest(ProductController.class)
@ExtendWith({RestDocumentationExtension.class})
class ProductControllerTest {

    @Autowired MockMvc mockMvc;
    @Autowired ObjectMapper objectMapper;
    @Autowired WebApplicationContext webApplicationContext;
    @MockBean ProductService productService;

    ProductDto testDto;
    String productDtoJsonString;

    @BeforeEach
    public void setUp(RestDocumentationContextProvider restDocumentation) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(documentationConfiguration(restDocumentation)).build();
    }
    @BeforeEach
    void createTestProduct() throws JsonProcessingException {
        testDto = ProductDto.builder()
                .id(null).version(null).createdDate(null).lastModifiedDate(null)
                .productName("testProductDto1")
                .productCategory(ProductCategory.VEGETABLE)
                .productPrice(new BigDecimal("3.5"))
                .eanId(ProductLoader.PRODUCT_1_EAN)
                .build();
        productDtoJsonString = objectMapper.writeValueAsString(testDto);
    }
    @AfterEach
    void tearDown() {
        reset(productService);
    }

    @DisplayName("listProducts Operations { ")
    @Nested
    class TestListOperations {
        @Captor
        ArgumentCaptor<String> productNameCaptor;
        @Captor
        ArgumentCaptor<ProductCategory> productCategoryCaptor;
        @Captor
        ArgumentCaptor<PageRequest> pageRequestCaptor;
        @Captor
        ArgumentCaptor<Boolean> showInventoryCaptor;

        @BeforeEach
        void createTestObjects() {
            List<ProductDto> productDtoList = new ArrayList<>();
            productDtoList.add(testDto);
            productDtoList.add(ProductDto.builder()
                    .id(null).version(null).createdDate(null).lastModifiedDate(null)
                    .productName("testProductDto2")
                    .productCategory(ProductCategory.VEGETABLE)
                    .eanId(ProductLoader.PRODUCT_2_EAN)
                    .currentStock(10)
                    .build());
            ProductPagedList productPagedList = new ProductPagedList(
                    productDtoList,
                    PageRequest.of(1, 1),
                    2L);
            given(
                    productService.listProducts(
                            productNameCaptor.capture(),
                            productCategoryCaptor.capture(),
                            pageRequestCaptor.capture(),
                            showInventoryCaptor.capture())
            ).willReturn(productPagedList);
        }
        @Test @DisplayName(" -> no parameters")
        void testListProducts() throws Exception {
            mockMvc.perform(get(ProductLoader.PRODUCT_PATH_V1).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content", hasSize(2)))
                .andExpect(jsonPath("$.content[0].productName", is("testProductDto1")))
                .andExpect(jsonPath("$.content[0].productCategory", is(ProductCategory.VEGETABLE.name())))
                .andExpect(jsonPath("$.content[0].eanId", is(testDto.getEanId())))
                .andExpect(jsonPath("$.content[0].productPrice", is(testDto.getProductPrice().toString())))
                .andExpect(jsonPath("$.content[1].productName", is("testProductDto2")))
                .andExpect(jsonPath("$.content[1].productCategory", is(ProductCategory.VEGETABLE.name())))
                .andExpect(jsonPath("$.content[1].eanId", is(ProductLoader.PRODUCT_2_EAN)))
                .andExpect(jsonPath("$.content[1].currentStock", is(10))
            ).andDo(document("v1/find-products"));
        }
        @Test @DisplayName(" -> all parameters")
        void testListProductsWithAllParameters() throws Exception {
            mockMvc.perform(get(ProductLoader.PRODUCT_PATH_V1)
                    .accept(MediaType.APPLICATION_JSON)
                    .param("number", "1")
                    .param("size", "25")
                    .param("productName", "testProductDto1")
                    .param("productCategory", "VEGETABLE")
                    .param("showInventory", "false")
            ).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content", hasSize(2)))
                .andExpect(jsonPath("$.content[0].productName", is("testProductDto1")))
                .andExpect(jsonPath("$.content[0].productCategory", is(ProductCategory.VEGETABLE.name())))
                .andExpect(jsonPath("$.content[0].eanId", is(testDto.getEanId())))
                .andExpect(jsonPath("$.content[0].productPrice", is(testDto.getProductPrice().toString())))
                .andExpect(jsonPath("$.content[1].productName", is("testProductDto2")))
                .andExpect(jsonPath("$.content[1].productCategory", is(ProductCategory.VEGETABLE.name())))
                .andExpect(jsonPath("$.content[1].eanId", is(ProductLoader.PRODUCT_2_EAN)))
                .andExpect(jsonPath("$.content[1].currentStock", is(10))
            ).andDo(document("v1/find-products-with-params",
                    requestParameters(
                            parameterWithName("number").description("Number of the page"),
                            parameterWithName("size").description("Elements on each page"),
                            parameterWithName("productName").description("Name of product"),
                            parameterWithName("productCategory").description("Product Category of product"),
                            parameterWithName("showInventory").description("Show inventory for product")
                    )));
        }
    }

    @Test
    void getProductById() throws Exception {
        given(productService.getProductByProductId(any(), anyBoolean())).willReturn(testDto);
        assertNotNull(testDto);

        mockMvc.perform(
                get(ProductLoader.PRODUCT_PATH_V1 + "{productId}", UUID.randomUUID())
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
        ).andExpect(status().isOk()
        ).andDo(document("v1/find-product-by-id",
                pathParameters(
                        parameterWithName("productId").description("UUID of the product")
                ),
                responseFields(
                    fieldWithPath("id").ignored(),
                    fieldWithPath("version").ignored(),
                    fieldWithPath("createdDate").ignored(),
                    fieldWithPath("lastModifiedDate").ignored(),
                    fieldWithPath("eanId").description("EAN of the product"),
                    fieldWithPath("productName").description("Name of the product"),
                    fieldWithPath("productCategory").description("Category of the product"),
                    fieldWithPath("productPrice").description("Price of the product"),
                    fieldWithPath("currentStock").description("The current inventory of the product - NULL WHEN NOT REQUESTED")
                )));
    }
    @Test
    void getProductByEanId() throws Exception {
        given(productService.getProductByEanId(any())).willReturn(testDto);
        assertNotNull(testDto);

        mockMvc.perform(
                get(ProductLoader.PRODUCT_EAN_PATH_V1 + "{eanId}", ProductLoader.PRODUCT_1_EAN)
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
        ).andExpect(status().isOk()
        ).andDo(document("v1/find-product-by-EAN",
                pathParameters(
                        parameterWithName("eanId").description("EAN of the product")
                ),
                responseFields(
                        fieldWithPath("id").ignored(),
                        fieldWithPath("version").ignored(),
                        fieldWithPath("createdDate").ignored(),
                        fieldWithPath("lastModifiedDate").ignored(),
                        fieldWithPath("eanId").description("EAN of the product"),
                        fieldWithPath("productName").description("Name of the product"),
                        fieldWithPath("productCategory").description("Category of the product"),
                        fieldWithPath("productPrice").description("Price of the product"),
                        fieldWithPath("currentStock").description("The current inventory of the product - NULL WHEN NOT REQUESTED")
                )));
    }
    @Test
    void createProduct() throws Exception {
        ConstrainedFields fields = new ConstrainedFields(ProductDto.class);

        given(productService.saveNewProduct(any())).willReturn(testDto);
        assertNotNull(testDto);

        mockMvc.perform(
                post(ProductLoader.PRODUCT_PATH_V1)
                    .contentType(MediaType.APPLICATION_JSON)
                    .characterEncoding("utf-8")
                    .content(productDtoJsonString)
        ).andExpect(status().isCreated()
        ).andDo(document("v1/create-product",
                requestFields(
                        fields.withPath("id").ignored(),
                        fields.withPath("version").ignored(),
                        fields.withPath("createdDate").ignored(),
                        fields.withPath("lastModifiedDate").ignored(),
                        fields.withPath("eanId").description("EAN of the product"),
                        fields.withPath("productName").description("Name of the product"),
                        fields.withPath("productCategory").description("Category of the product"),
                        fields.withPath("productPrice").description("Price of the product"),
                        fields.withPath("currentStock").ignored().description("The current inventory of the product - THIS IS NULL WHEN NOT REQUESTED")
                ),
                responseFields(
                        fieldWithPath("id").ignored(),
                        fieldWithPath("version").ignored(),
                        fieldWithPath("createdDate").ignored(),
                        fieldWithPath("lastModifiedDate").ignored(),
                        fieldWithPath("eanId").description("EAN of the product"),
                        fieldWithPath("productName").description("Name of the product"),
                        fieldWithPath("productCategory").description("Category of the product"),
                        fieldWithPath("productPrice").description("Price of the product"),
                        fieldWithPath("currentStock").description("The current inventory of the product - NULL WHEN NOT REQUESTED")
                )));
    }
    @Test
    void updateProduct() throws Exception {
        ConstrainedFields fields = new ConstrainedFields(ProductDto.class);

        given(productService.updateProduct(any(), any())).willReturn(testDto);
        assertNotNull(testDto);

        mockMvc.perform(
                put(ProductLoader.PRODUCT_PATH_V1 + "{productId}", UUID.randomUUID())
                    .contentType(MediaType.APPLICATION_JSON)
                    .characterEncoding("utf-8")
                    .content(productDtoJsonString)
        ).andExpect(status().isNoContent()
        ).andDo(document("v1/update-product",
                pathParameters(
                        parameterWithName("productId").description("UUID of product")
                ),
                requestFields(
                        fields.withPath("id").ignored(),
                        fields.withPath("version").ignored(),
                        fields.withPath("createdDate").ignored(),
                        fields.withPath("lastModifiedDate").ignored(),
                        fields.withPath("eanId").description("EAN of the product"),
                        fields.withPath("productName").description("Name of the product"),
                        fields.withPath("productCategory").description("Category of the product"),
                        fields.withPath("productPrice").description("Price of the product"),
                        fields.withPath("currentStock").ignored().description("The current inventory of the product - THIS IS NULL WHEN NOT REQUESTED")
                ),
                responseFields(
                        fieldWithPath("id").ignored(),
                        fieldWithPath("version").ignored(),
                        fieldWithPath("createdDate").ignored(),
                        fieldWithPath("lastModifiedDate").ignored(),
                        fieldWithPath("eanId").description("EAN of the product"),
                        fieldWithPath("productName").description("Name of the product"),
                        fieldWithPath("productCategory").description("Category of the product"),
                        fieldWithPath("productPrice").description("Price of the product"),
                        fieldWithPath("currentStock").description("The current inventory of the product - NULL WHEN NOT REQUESTED")
                )));
    }

    private static class ConstrainedFields {
        /* Extracts metadata from constraints on properties of input Class<?> */
        private final ConstraintDescriptions constraintDescriptions;
        ConstrainedFields(Class<?> input) {
            this.constraintDescriptions = new ConstraintDescriptions(input);
        }
        private FieldDescriptor withPath(String path) {
            return fieldWithPath(path)
                    .attributes(key("constraints").value(
                            StringUtils.collectionToDelimitedString(
                                    this.constraintDescriptions.descriptionsForProperty(path), ". "
                            )));
        }
    }

}
