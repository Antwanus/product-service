package com.antoonvereecken.productservice.domain;

import com.antoonvereecken.common.model.ProductCategory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@Tag("model")
@DisplayName("Product { ")
class ProductTest {

    Product p;
    String eanId = "1234567890123";
    String testName = "testName";
    String testCategory = "vegetable";
    BigDecimal testPrice = BigDecimal.valueOf(3.5);
    Integer testMinStock = 1;
    Integer testProductionQuantity = 10;

    @BeforeEach
    void createTestObject() {
        p = Product.builder()
                .id(UUID.randomUUID())
                .version(1L)
                .createdDate(Timestamp.from(Instant.now()))
                .lastModifiedDate(Timestamp.from(Instant.now()))
                .eanId(eanId)
                .productName(testName)
                .productCategory(ProductCategory.valueOf(testCategory.toUpperCase()))
                .productPrice(testPrice)
                .minStock(testMinStock)
                .productionQuantity(testProductionQuantity)
                .build();
    }

    @Test
    void testProductModel() {
        assertAll("Test properties after object creation",
                () -> assertNotNull(p, "PRODUCT OBJECT SHOULD NOT BE NULL"),
                () -> assertNotNull(p.getId(), "ID FAILED"),
                () -> assertNotNull(p.getVersion(), "VERSION FAILED"),
                () -> assertNotNull(p.getCreatedDate(), "ID SHOULD NOT BE NULL"),
                () -> assertNotNull(p.getLastModifiedDate(), "LAST_MODIFIED_DATE FAILED"),
                () -> assertEquals(p.getEanId(), eanId, "EAN_ID FAILED"),
                () -> assertEquals(p.getProductName(), testName, "PRODUCT_NAME FAILED"),
                () -> assertEquals(p.getProductCategory(), ProductCategory.valueOf(testCategory.toUpperCase()), "PRODUCT_CATEGORY FAILED"),
                () -> assertEquals(p.getProductPrice(), testPrice, "PRICE FAILED"),
                () -> assertEquals(p.getMinStock(), testMinStock, "MIN_STOCK FAILED"),
                () -> assertEquals(p.getProductionQuantity(), testProductionQuantity, "PRODUCTION_QTY FAILED")
        );


    }
}