This app provides product data.

There is a docker-compose.yml (/product-service/src/main/docker/local) that pulls all the images from
my DockerHub & builds containers for all the services.
This composition uses a customised MySql image, on MySQL8.0.18 + initialises a schema for
product-, inventory- & order-service. (creates DB & user/privileges)

There is a "config-server"-service that uses an app.properties from my GitHub. It searches for the correct file
according to your service name & profile settings (check environment in docker-compose.yml)

-> you can comment out product-, inventory- & order-service if you want these to run in your IDE.
Be aware that you might have a SQL server running on your host 3306 (you can comment out mysql-service
& adminer-service if you want to use your server).
This is why you access MYSQL container from your host on port 3307 & inside Docker on 3306.

----------------------------------------------------------------
|   INFO                                                       |
----------------------------------------------------------------
This app requires ArtemisMQ for JMS.
-> docker run -it --rm -p 8161:8161 -p 61616:61616 vromero/activemq-artemis
-> DEFAULT CREDS
        login: artemis
        pw: simetraehcapa

This app requires Zipkin for distributed tracing
-> docker run -d -p 9411:9411 openzipkin/zipkin

This app requires Eureka server for service-discovery
-> product-services-eureka
-> port 8761

This app requires Config server for fetching ENV
-> product-services-config-server
    -> fetching from github/Antwanus/product-config-repo

This app depends on an inventory service
-> product-inventory-service